MAD april 2017
=====
Repository containing all the working files used for the MAD opening 

including:

- Normographs svg files for the [Belgika](http://gitlab.constantvzw.org/osp/foundry.belgica-belgika.git) and [ax28](http://gitlab.constantvzw.org/osp/foundry.ax-28-script.git)
- Overprinting correction file for the [Fluxisch Else](https://gitlab.constantvzw.org/osp/foundry.fluxisch-else.git)
- Cartel files
-"Ready to cut" Package for Crickx precut sentences : svg files+font files
        